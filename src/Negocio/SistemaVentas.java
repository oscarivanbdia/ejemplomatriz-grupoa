/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Vendedor;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 * Clase del sistema de ventas
 * @author madarme
 */
public class SistemaVentas {
    
    private Vendedor equipoVentas[];
    private float promTotal;
    
    public SistemaVentas() {
     
    }

    public SistemaVentas(int numVendedores) {
        
        this.equipoVentas=new Vendedor[numVendedores];
     
    }
    
    /**
     *  Constructor que carga los vendedores a partir de un excel
     * @param rutaArchivo un string con la ruta y el nombre del archivo Ejemplo: src/Datos/vendedores.xls
     * @throws IOException Genera excepción cuando el archivo no existe
     */
    
    public SistemaVentas(String rutaArchivo) throws IOException
    {
        LeerMatriz_Excel myExcel=new LeerMatriz_Excel(rutaArchivo,0);
        String datos[][]=myExcel.getMatriz();
        
        // Normalizar --> Pasar de la matriz de String al modelo del negocio (equipoVentas con cada uno de sus vendedores
        this.equipoVentas=new Vendedor[datos.length-1];
        crearVendedores(datos);
    
    }
    
    
    private void crearVendedores(String datos[][])
    {
     float promVentasTotal = 0.0f;
     for(int fila=1;fila<datos.length;fila++)
     {
         //Crear un Vendedor
         Vendedor nuevo=new Vendedor();
         //Vector de ventas: Creando el espacio
         float ventas[]=new float[datos[fila].length-2];
         float promVentas=0.0f;
         
         int indice_venta=0;
         
         for(int columna=0;columna<datos[fila].length;columna++)
         {
         
             if(columna==0)
                 nuevo.setCedula(Long.parseLong(datos[fila][columna]));
             else
             {
                 if(columna==1)
                     nuevo.setNombre(datos[fila][columna]);
                 else //Default
                 {
                     float ventaX = Float.parseFloat(datos[fila][columna]);
                     ventas[indice_venta]=ventaX;
                     indice_venta++;
                     promVentas += ventaX;
                 }
             }
             
         }
         nuevo.setAcumuladoVentas(promVentas);
         promVentas= promVentas/(indice_venta*1.0f);
         nuevo.setPromVentas(promVentas);
         promVentasTotal += promVentas;
         //Asignar el vector de ventas al vendedor:
         nuevo.setVentas(ventas);
         //Asingar el nuevo vendedor al equipo de vendedores:
         this.equipoVentas[fila-1]=nuevo;
      }
      this.promTotal = promVentasTotal/(datos.length-1);
    }
    
    public Vendedor[] getEquipoVentas() {
        return equipoVentas;
    }

    public void setEquipoVentas(Vendedor[] equipoVentas) {
        this.equipoVentas = equipoVentas;
    }

    @Override
    public String toString() {
        
        String msg="";
            for(Vendedor myVendedor:this.equipoVentas)
                     msg+=myVendedor.toString()+"\n";
        
        return msg;
        
    }
    
    /**
     * Una colección de vendedores que obtuvieron ventas mayores al promedio total de ventas
     * @return  una colección de vendedores
     */
    public Vendedor[] getVendedores_MasVentas()
    {
        Vendedor[] vendedores = new Vendedor[equipoVentas.length];
        int indice=0;
        for(int i=0;i<equipoVentas.length;i++)
        {
            if (equipoVentas[i].getPromVentas() > this.promTotal){
                vendedores[indice] = equipoVentas[i];
                indice++;
            }     
        }
        return vendedores;
    }
//    public float getPromTotal()
//    {
//        float prom=0.0f;
//        for(int i=0;i<equipoVentas.length;i++){
//
//        }
//        return prom;
//    }
    
    /**
     * Obtiene el nombre de la venta que obtuvo la menor ganancia
     * venta1, ... venta6
     * @return 
     */
    public String getVenta_Menor()
    {
        float acumVentas[] =new float[equipoVentas[0].getVentas().length];
        String ventaMenor = "venta1";
        for(int i=0;i<equipoVentas.length;i++)
        {
            float ventas[]= equipoVentas[i].getVentas();
            for(int j=0;j<ventas.length;j++)
            {
                acumVentas[j] += ventas[j];
            }
        }
        float mVenta = acumVentas[0];
        for(int i=0;i<acumVentas.length;i++)
        {
            if(acumVentas[i]<mVenta){
                mVenta = acumVentas[i];
                ventaMenor = "venta"+(i+1);
            }
        }
        return ventaMenor;
    }
    
   /**
    * Obtiene el vendedor que obtuvo la menor venta acumulada
    * @return un objeto de tipo vendedor
    */
    public Vendedor getMenosVentas()
    {   
        Vendedor menosVentas = equipoVentas[0];
        for(int i=0;i<equipoVentas.length;i++){
            if(equipoVentas[i].getAcumuladoVentas()<menosVentas.getAcumuladoVentas())
                menosVentas =  equipoVentas[i];
        }
        return menosVentas;
    }
    
}
