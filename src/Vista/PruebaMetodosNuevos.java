/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.Vendedor;
import Negocio.SistemaVentas;
import java.io.IOException;
/**
 *
 * @author madar
 */
public class PruebaMetodosNuevos {
    
    public static void main(String[] args) throws IOException {
        SistemaVentas mySistema=new SistemaVentas("src/Datos/vendedores.xls");
        System.out.println("Mi sistema tiene los vendedores: \n"+mySistema.toString());
        Vendedor[] masVentas = mySistema.getVendedores_MasVentas();
        System.out.println("Los vendores con mas ventas fueron: \n");
        for(int i=0;i<masVentas.length && masVentas[i]!=null;i++){
          System.out.println(masVentas[i].getNombre());
        }
        System.out.println("La venta que obtuvo la menor ganancia: \n");
        System.out.println(mySistema.getVenta_Menor());
        System.out.println("El vendedor que obtuvo la menor venta acumulada: \n");
        System.out.println(mySistema.getMenosVentas().getNombre());
    }
}
